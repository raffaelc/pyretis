2D WCA with LAMMPS
==================

This is an example of using PyRETIS and LAMMPS for performing
a RETIS simulation. This example is described on the web-page
and the description can be found either in the documentation
source:

`docs/examples/examples-lammps-wca.rst`

Or on the web-page in the `Getting started` section.

Please note that you will have to generate an initial
trajectory before running the RETIS simulation. How
this can be done, is also discussed in the documentation
for this particular example.
