# Cycle: 0, status: ACC
#     Time      Potential        Kinetic
         0            nan            nan
         1            nan            nan
         2            nan            nan
         3            nan            nan
         4            nan            nan
         5            nan            nan
         6            nan            nan
         7            nan            nan
         8            nan            nan
         9            nan            nan
        10            nan            nan
        11            nan            nan
        12            nan            nan
        13            nan            nan
        14            nan            nan
        15            nan            nan
        16            nan            nan
        17            nan            nan
        18            nan            nan
        19            nan            nan
        20            nan            nan
        21            nan            nan
        22            nan            nan
        23            nan            nan
        24            nan            nan
        25            nan            nan
        26            nan            nan
        27            nan            nan
        28            nan            nan
        29            nan            nan
        30            nan            nan
        31            nan            nan
        32            nan            nan
        33            nan            nan
        34            nan            nan
        35            nan            nan
        36            nan            nan
        37            nan            nan
        38            nan            nan
        39            nan            nan
        40            nan            nan
# Cycle: 1, status: ACC
#     Time      Potential        Kinetic
         0      -1.131914       0.020961
         1      -1.112982       0.002375
         2      -1.129501       0.018875
# Cycle: 2, status: ACC
#     Time      Potential        Kinetic
         0            nan            nan
         1            nan            nan
         2            nan            nan
         3            nan            nan
         4            nan            nan
         5            nan            nan
         6            nan            nan
         7            nan            nan
         8            nan            nan
         9            nan            nan
        10            nan            nan
        11            nan            nan
        12            nan            nan
        13            nan            nan
        14            nan            nan
        15            nan            nan
        16            nan            nan
        17            nan            nan
        18            nan            nan
        19            nan            nan
        20            nan            nan
        21            nan            nan
        22            nan            nan
        23            nan            nan
        24            nan            nan
        25            nan            nan
        26            nan            nan
        27            nan            nan
        28            nan            nan
        29            nan            nan
        30            nan            nan
        31            nan            nan
        32            nan            nan
        33            nan            nan
        34            nan            nan
        35            nan            nan
        36            nan            nan
        37            nan            nan
        38            nan            nan
        39            nan            nan
        40            nan            nan
# Cycle: 3, status: ACC
#     Time      Potential        Kinetic
         0            nan            nan
         1      -1.110956       0.044667
         2      -1.103428       0.037197
         3      -1.131464       0.064892
# Cycle: 4, status: ACC
#     Time      Potential        Kinetic
         0      -1.131914       0.020961
         1      -1.112982       0.002375
         2      -1.129501       0.018875
# Cycle: 5, status: ACC
#     Time      Potential        Kinetic
         0      -1.131838       0.020873
         1      -1.112982       0.002375
         2      -1.129311       0.018698
