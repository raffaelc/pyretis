Particles
---------
position = {'generate': 'sq',
            'repeat': [6, 6],
            'lcon': 1.0}
name = ['Ar']
mass = {'Ar': 1.0}

System
------
dimensions = 3
units = lj
