TIS results for ensemble: @{{ ensemble }}@
===============================

@{{ tables['interfaces'] }}@

@{{ tables['probability'] }}@

@{{ tables['path'] }}@

@{{ tables['efficiency'] }}@
